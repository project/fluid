<?php
include_once('template.inc');



/**
 * Implementation of hook_regions
 */
function fluid_regions() {
  return array(
    'header' => t('header'),
    'shortcuts' => t('shortcuts'),
    'left' => t('left sidebar'),
    'content_top' => t('content top'),
    'content_bottom' => t('content bottom'),
    'right' => t('right sidebar'),
    'footer' => t('footer'),
    'extra_0' => t('extra 0'),
    'extra_1' => t('extra 1'),
  );
}



// --------------------------------------------------------------- Theming hooks



/**
 * Custom logo themeing hook
 */
function fluid_logo($url){
  return 
    '<div id="logo">
      <a href="/" title="'.t('Home').'">
        <img src="'.$url.'" alt="'.t('Home').'"/>
      </a>
    </div>';
}



/**
 * Custom shortcut theming
 */
function fluid_shortcuts($primary_links, $secondary_links){
  $rendered = '';
  
  if ($primary_links || $secondary_links){
    $rendered = '<div id="shortcuts">';
    $rendered .= $primary_links 
      ? '<div id="primary-links">'.theme('menu_links', $primary_links).'</div>'
      : '';
    $rendered .= $secondary_links 
      ? '<div id="secondary-links">'.
        theme('menu_links', $secondary_links).
        '</div>'
      : '';
    $rendered .= '</div><!-- shortcuts -->';
  }
  
  return $rendered;  
}



/**
 * Custom breadcrumb themeing hook
 */
function fluid_breadcrumb($breadcrumb){
  return $breadcrumb && !drupal_is_front_page() && is_array($breadcrumb) 
    ? '<div id="bread-crumb">'.implode(' &gt; ', $breadcrumb).'</div>' 
    : '';
}



/**
 * Implementation of theme_textfield
 * Removes the #size attribute
 */
function fluid_textfield($element){
  unset($element['#size']);
  return theme_textfield($element);
}
/**
 * Implementation of theme_password
 * Removes the #size attribute
 */
function fluid_password($element){
  unset($element['#size']);
  return theme_password($element);
}
/**
 * Implementation of theme_select
 * Removes the #size attribute
 */
function fluid_select($element){
  unset($element['#size']);
  return theme_select($element);
}



/**
 * Implementation of theme_block
 */
function fluid_block($block) {
  
  $theme_variables = _get_theme_variables();
  
  $suggestions[] = 'block';
  $suggestions[] = 'block-' . $block->region;
  $suggestions[] = 'block-' . $block->module;
  $suggestions[] = 'block-' . $block->module . '-' . $block->delta;

  if ($theme_variables['path'] != $theme_variables['parent_path']){
    $suggestions[] = 'sub-themes/'.$theme_variables['key'].'/block';
    $suggestions[] = 'sub-themes/'.$theme_variables['key'].'/block-' . $block->region;
    $suggestions[] = 'sub-themes/'.$theme_variables['key'].'/block-' . $block->module;
    $suggestions[] = 'sub-themes/'.$theme_variables['key'].'/block-' . $block->module . '-' . $block->delta;
  }
  
  $id = ereg_replace('[\ ]+', '-', 
          ereg_replace('[^a-z0-9\ ]*', '', 
            strtolower(
              trim(
                htmlspecialchars_decode($block->subject, ENT_QUOTES)
              )
            )
          )
        );
  
  $show_title = !ereg('^\[.*\]$', $block->subject); 
        
  $variables = array(
    'css_id' => $id,
    'show_title' => $show_title,
    'block' => $block
  );
        
  return _phptemplate_callback('block', $variables, $suggestions);
}



/**
 * Implementation of theme_button
 */
function fluid_button($element) {
  if (isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] = 'form-'. $element['#button_type'] .' '. $element['#attributes']['class'];
  }
  else {
    $element['#attributes']['class'] = 'form-'. $element['#button_type'];
  }
  return '<input type="'.
    (($element['#button_type'] == "image") ? 'image' : 'submit' ).
    '" '.(empty($element['#name']) ? '' : 'name="'.$element['#name'].'" ').
    'id="'.$element['#id'].'" value="'. check_plain($element['#value']).'" '.
    drupal_attributes($element['#attributes']) ." />\n";
}



/**
 * Implementation of theme_menu_item_link
 */
function fluid_menu_item_link($mid, $item, $class = '') {
  $attributes = !empty($item['description']) ? array('title' => $item['description']) : array();
  $attributes['class'] .= $class;
  $item['title'] = '<span>'.$item['title'].'</span>';
  return l($item['title'], $item['path'], $attributes, isset($item['query']) ? $item['query'] : NULL, NULL, FALSE, TRUE);
}
/**
 * Implementation of theme_menu_item
 */
function fluid_menu_item($mid, $children = '', $leaf = TRUE) {
  $item = menu_get_item($mid);
    
  $id = ereg_replace('[\ ]+', '-', 
          ereg_replace('[^a-z0-9\ ]*', '', 
            strtolower(
              trim(
                htmlspecialchars_decode($item['title'], ENT_QUOTES)
              )
            )
          )
        );
        
  $id = ($id ? $id : $mid);
  $class = 
    'menu-'.$id.' '.
    ($leaf ? 'leaf' : ($children ? 'expanded' : 'collapsed'));
  
  return 
    "<li class=\"$class\">". 
      theme('menu_item_link', $mid, $item, $class).
      $children.
    "</li>\n";
}