<?php
// This needs to change for drupal 6, as the path to theme returns the path to the subtheme
include_once(path_to_theme().'/theme-settings.php');



/* ------------------------------------------------- Form */



/**
 * Implementation of hook_settings
 */
function dry_fluid_settings($saved_settings) {
  return fluid_settings($saved_settings);  
}