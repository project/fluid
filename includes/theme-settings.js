// Hide the custom fields

function checkCustomField(element){
  var target = $('#' + element.id + '-custom'); 
  if (element.value=='custom'){
    target.removeAttr('disabled');
    target.parent().css('display', 'block');
  } else {
    target.attr('disabled', 'disabled');
    target.parent().css('display', 'none');
  }
}

$(document).ready(
  function(){

    // check custom field visibility/editability on change
    $('.has-custom-field').change(function(){ checkCustomField(this);});
    
    // Hide disabled fields
    $(':disabled').parent().css('display', 'none');
    $('.error').removeAttr('disabled').parent().css('display', 'block');
    
    // Init pickers and float to the right
    $('.color-picker').each(
      function(){
        $(this).farbtastic();
        $(this).css('float', 'right');
      }
    );
    
    $('.has-color-picker').each(
      function(){
        var pickerId = $(this).parent().parent().children('.color-picker').attr('id');
        $.farbtastic('#' + pickerId).linkTo('#' + this.id);
      }
    );
    
    // Link field to color picker on focus
    $('.has-color-picker').focus(
      function(){
        var pickerId = $(this).parent().parent().children('.color-picker').attr('id');
        $.farbtastic('#' + pickerId).linkTo('#' + this.id);
      }
    );
    
    // Focus color textbox on enabling
    $('.use-color').click(
      function(){
        $(this).parent().parent().next().children('.has-color-picker').focus();
      }
    );
    
  }
);