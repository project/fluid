/* ------------------------------- Structure */


/* --Global -- */
body{
  margin: <?php print $body_margin 
    ? $body_margin 
    : '0px'; ?>;
  padding: <?php print $body_padding && !$body_border 
    ? $body_padding 
    : '0px'; ?>;
}
<?php if ($body_border): ?>
body div.inner{ padding: <?php print $body_padding 
  ? $body_padding 
  : '0px;'; ?>;}
<?php endif; ?>
<?php if ($body_border) foreach ($body_border as $element=>$properties){?>
body div.<?php print $element ?> { background: transparent url('<?php 
    print $properties['url']; ?>') <?php 
    print $properties['repeat'] ?> <?php 
    print $properties['position'] ?>;}
<?php }?>


/* -- Page -- */
div#center{
  margin-left: auto; margin-right: auto;
<?php if ($custom_width): ?>
  width: <?php print $page_width; ?>px;
<?php endif; ?>
}

div#page{
  float: left;
  margin: <?php print $page_margin ? $page_margin : '0px;'; ?>;
  padding: <?php print $page_padding && !$page_border 
    ? $page_padding 
    : '0px'; ?>;
<?php if ($custom_width): ?>
  width: <?php print $page_width; ?>px;
<?php endif; ?>
}
<?php if ($page_border): ?>
div.page-inner{ padding: <?php print $page_padding ? $page_padding : '0px'; ?>;}
<?php endif; ?>
<?php if ($page_border) foreach ($page_border as $element=>$properties){?>
div.page-<?php print $element ?> { background: transparent url('<?php 
    print $properties['url']; ?>') <?php 
    print $properties['repeat'] ?> <?php 
    print $properties['position'] ?>;}
<?php }?>


/* --- Page header */
div#page div#page-header{
  clear: both;
  margin: <?php print $header_margin ? $header_margin : '0px'; ?>;
  padding: <?php print $header_padding && !$header_border 
    ? $header_padding 
    : '0px'; ?>;
}
<?php if ($header_border): ?>
div.header-inner{ padding: <?php print $header_padding 
  ? $header_padding 
  : '0px'; ?>;}
<?php endif; ?>
<?php if ($header_border) foreach ($header_border as $element=>$properties){?>
div.header-<?php print $element ?> { background: transparent url('<?php 
  print $properties['url']; ?>') <?php 
  print $properties['repeat'] ?> <?php 
  print $properties['position'] ?>;}
<?php }?>


/* -- Page body -- */
div#page div#page-body{
  width: 100%;
}

div#page div#page-body div.spacing{
  margin: <?php print $page_body_margin ? $page_body_margin : '0px'; ?>;
<?php if (!$page_body_border): ?>
  padding: <?php print $page_body_padding ? $page_body_padding : '0px'; ?>;
<?php endif; ?>
}
div#page div#page-body div#page-body-columns{
  width: 100%;
}
<?php if ($page_body_border): ?>
div.page-body-inner{ padding: <?php print $page_body_padding 
  ? $page_body_padding 
  : '0px'; ?>;}
<?php foreach ($page_body_border as $element=>$properties){?>
div.page-body-<?php print $element ?> { background: transparent url('<?php 
  print $properties['url']; ?>') <?php 
  print $properties['repeat'] ?> <?php 
  print $properties['position'] ?>;}
<?php }?>
<?php endif;?>


/* -- Side columns -- */
div#page div#page-body div.side div.spacing{
  margin: <?php print $column_margin 
    ? $column_margin 
    : '0px'; ?>; padding: 0px;
  padding: <?php print $column_padding && !$column_border 
    ? $column_padding 
    : '0px'; ?>;
}

<?php if ($column_border): ?>
div.column-inner{ padding: <?php print $column_padding 
  ? $column_padding 
  : '0px'; ?>;}
<?php endif;?>
<?php if ($column_border) foreach ($column_border as $element=>$properties){?>
div.column-<?php print $element ?> { background: transparent url('<?php 
  print $properties['url']; ?>') <?php 
  print $properties['repeat'] ?> <?php 
  print $properties['position'] ?>;}
<?php }?>


div#page div#page-body div#first-column{
  float: <?php print $left_column_float; ?>;
  width: <?php print $column_width; ?>px;
<?php if ($first_column_margin): ?>
  margin: <?php print $first_column_margin; ?>;
<?php endif; ?> 
}

div#page div#page-body div#second-column{
  float: <?php print $right_column_float; ?>;
  width: <?php print $column_width; ?>px;
<?php if ($second_column_margin): ?>
  margin: <?php print $second_column_margin; ?>;
<?php endif; ?> 
}

div#page div#sidebars{
<?php if ($sidebars_float): ?>
  float: <?php print $sidebars_float; ?>;
  width: <?php print $column_container_width; ?>px;
<?php endif;?>
  margin: <?php print $sidebars_margin; ?>;
  padding: 0px;
}


/* -- Content -- */


<?php if ($content_margin): ?>
div#page div#page-body div#content div.spacing{
  margin: <?php print $content_margin; ?>; padding: 0px;
}
<?php endif; ?>

div#page div#page-body div#content{
<?php if ($content_float):?>float: <?php print $content_float; ?>;<?php endif; ?>
  margin: <?php print $content_layout_margin; ?>;
  padding: <?php print $content_padding && !$content_border 
    ? $content_padding 
    : '0px'; ?>;
}
<?php if ($content_border): ?>
div.content-inner{ padding: <?php print $content_padding 
  ? $content_padding 
  : '0px'; ?>;}
<?php endif;?>
<?php if ($content_border) foreach ($content_border as $element=>$properties){?>
div.content-<?php print $element ?> { background: transparent url('<?php 
  print $properties['url']; ?>') <?php 
  print $properties['repeat'] ?> <?php 
  print $properties['position'] ?>;}
<?php }?>


/* -- Blocks -- */
div#page div.block{
  margin: <?php print $block_margin ? $block_margin : '0px'; ?>;
  padding: <?php print $block_padding && !$block_border 
    ? $block_padding 
    : '0px'; ?>;
}
<?php if ($block_border): ?>
div.block-inner{ padding: <?php print $block_padding 
  ? $block_padding 
  : '0px'; ?>;}
<?php endif; ?>
<?php if ($block_border) foreach ($block_border as $element=>$properties){?>
div.block-<?php print $element ?> { background: transparent url('<?php 
  print $properties['url']; ?>') <?php 
  print $properties['repeat'] ?> <?php 
  print $properties['position'] ?>;}
<?php }?>


/* --- Page footer -- */
div#page div#page-footer{
  clear: both; 
  margin: <?php print $footer_margin ? $footer_margin : '0px'; ?>;
  padding: <?php print $footer_padding && !$footer_border 
    ? $footer_padding 
    : '0px'; ?>;
}
<?php if ($footer_border): ?>
div.footer-inner{ padding: <?php print $footer_padding 
  ? $footer_padding 
  : '0px'; ?>;}
<?php endif;?>
<?php if ($footer_border) foreach ($footer_border as $element=>$properties){?>
div.footer-<?php print $element ?> { background: transparent url('<?php 
  print $properties['url']; ?>') <?php 
  print $properties['repeat'] ?> <?php 
  print $properties['position'] ?>;}
<?php }?>