<?php



/**
 * Returns theme related variables
 */
function _get_theme_variables(){

  global $theme;
  global $theme_key;
  
  $theme_path = path_to_theme();
  $subtheme_path = path_to_theme().'/sub-themes/'.$theme_key;
  if (strpos(' '.$theme_path, $theme_key) == 0 && file_exists(realpath($subtheme_path))){
    $theme_path = $subtheme_path;
  }
  
  return array(
    'theme' => $theme,
    'key' => $theme_key,
    'path' => $theme_path,
    'prefix' => $theme_key.'_',
    'settings' => theme_get_settings($theme_key),
    'parent_path' => strrpos($theme_path, '/sub-themes')
      ? substr($theme_path, 0, strrpos($theme_path, '/sub-themes'))
      : $theme_path,
  );
}

/**
 * Prints debug output
 */
function _print_debug($to_print, $label = FALSE){
  $backtrace = debug_backtrace();
  print '<pre>';
  print '[ ----- function '.$backtrace[1]['function'].'(...) ----- ]'."\n";
  print ($label ? $label.' : ' : ''); 
  print_r($to_print); print '</pre>';
}


/* ------------------------------------------------- Customisable elements */



function _fluid_custom_elements(){
  return array(
  
    'body' => array(
      'section title' => 'Body',
      'title' => FALSE,
      'border' => TRUE,
      'margin' => TRUE,
      'padding' => TRUE,
      'color' => array(
        'background' => array(
          'title' => 'Background', 
          'selector' => 'body',
          'property' => 'background-color',
        ),
        'text' => array(
          'title' => 'Text',
          'selector' => 'body',
          'property' => 'color',
        ),
        'link' => array(
          'title' => 'Link',
          'selector' => 'body a',
          'property' => 'color',
        ),
        'link_hover' => array(
          'title' => 'Link hover',
          'selector' => 'body a:hover',
          'property' => 'color',
        ),
        'heading' => array(
          'title' => 'Headings', 
          'selector' => 'body h1, body h2, body h3, body h4, body h5, body h5',
          'property' => 'color',
        ),
      ),
      'variable context' => 'page',
    ),
    
    'page' => array(
      'section title' => 'Page',
      'title' => FALSE,
      'border' => TRUE,
      'margin' => TRUE,
      'padding' => TRUE,
      'color' => array(
        'background' => array(
          'title' => 'Background', 
          'selector' => 'div#page',
          'property' => 'background-color',
        ),
      ),
      'variable context' => 'page',
    ),
    
    'page-body' => array(
      'section title' => 'Page body',
      'title' => FALSE,
      'border' => TRUE,
      'margin' => TRUE,
      'padding' => TRUE,
      'color' => array(
        'background' => array(
          'title' => 'Background', 
          'selector' => 'div#page-body div.page-body-outer',
          'property' => 'background-color',
        ),
      ),
      'variable context' => 'page',
      'uses spacer' => TRUE,
    ),
    
    'header' => array(
      'section title' => 'Page header',
      'title' => FALSE,
      'border' => TRUE,
      'margin' => TRUE,
      'padding' => TRUE,
      'color' => array(
        'background' => array(
          'title' => 'Background', 
          'selector' => 'div#page-header',
          'property' => 'background',
        ),
        'text' => array(
          'title' => 'Text', 
          'selector' => 'div#page-header',
          'property' => 'color',
        ),
        'link' => array(
          'title' => 'Link', 
          'selector' => 'div#page-header a',
          'property' => 'color',
        ),
        'link_hover' => array(
          'title' => 'Link hover', 
          'selector' => 'div#page-header a:hover',
          'property' => 'color',
        ),
      ),
      'variable context' => 'page',
    ),
    
    'content' => array(
      'section title' => 'Content area',
      'title' => TRUE,
      'border' => TRUE,
      'margin' => TRUE,
      'padding' => TRUE,
      'color' => array(
        'background' => array(
          'title' => 'Background',
          'selector' => 'div#content',
          'property' => 'background-color',
        ),
        'text' => array(
          'title' => 'Text', 
          'selector' => 'div#content',
          'property' => 'color',
        ),
        'link' => array(
          'title' => 'Link', 
          'selector' => 'div#content a',
          'property' => 'color',
        ),
        'link_hover' => array(
          'title' => 'Link hover', 
          'selector' => 'div#content a:hover',
          'property' => 'color',
        ),
        'heading' => array(
          'title' => 'Heading',
          'selector' => 'div#content h1, div#content h2, div#content h3, div#content h4, div#content h5, div#content h6',
          'property' => 'color',
        ),
      ),
      'variable context' => 'page',
    ),
    
    'column' => array(
      'section title' => 'Columns',
      'title' => FALSE,
      'border' => TRUE,
      'margin' => TRUE,
      'padding' => TRUE,
      'color' => array(
        'background' => array(
          'title' => 'Background', 
          'selector' => 'div.side',
          'property' => 'background-color',
        ),
      ),
      'variable context' => 'page',
      'uses spacer' => TRUE,
    ),
    
    'block' => array(
      'section title' => 'Blocks',
      'title' => TRUE,
      'border' => TRUE,
      'margin' => TRUE,
      'padding' => TRUE,
      'color' => array(
        'background' => array(
          'title' => 'Background', 
          'selector' => 'div.block',
          'property' => 'background-color',
        ),
        'content_background' => array(
          'title' => 'Content background', 
          'selector' => 'div.block div.content',
          'property' => 'background-color',
        ),
        'text' => array(
          'title' => 'Text', 
          'selector' => 'div.block',
          'property' => 'color',
        ),
        'link' => array(
          'title' => 'Link',
          'selector' => 'div.block a',
          'property' => 'color',
        ),
        'link_hover' => array(
          'title' => 'Link hover', 
          'selector' => 'div.block a:hover',
          'property' => 'color',
        ),
        'heading' => array(
          'title' => 'Heading', 
          'selector' => 'div.block h1, div.block h2, div.block h3, div.block h4, div.block h5, div.block h6',
          'property' => 'color',
        ),
      ),
      'variable context' => 'block',
    ),
    
    'footer' => array(
      'section title' => 'Page footer',
      'title' => FALSE,
      'border' => TRUE,
      'margin' => TRUE,
      'padding' => TRUE,
      'color' => array(
        'background' => array(
          'title' => 'Background', 
          'selector' => 'div#page-footer',
          'property' => 'background-color',
        ),
        'text' => array(
          'title' => 'Text',
          'selector' => 'li#footer',
          'property' => 'color',
        ),
        'link' => array(
          'title' => 'Link',
          'selector' => 'div#page-footer a',
          'property' => 'color',
        ),
        'link_hover' => array(
          'title' => 'Link hover', 
          'selector' => 'div#page-footer a:hover',
          'property' => 'color',
        ),
        'heading' => array(
          'title' => 'Heading',
          'selector' => 'div#page-footer h1, div#page-footer h2, div#page-footer h3, div#page-footer h4, div#page-footer h5, div#page-footer h6',
          'property' => 'color',
        ),
      ),
      'variable context' => 'page',
    ),
    
  );
}



/* ------------------------------------------------- Custom fields */



function _fluid_custom_field(){
  return array('class' => 'custom-field');
}

function _fluid_has_custom_field(){
  return array('class' => 'has-custom-field');
}

function _fluid_custom_field_disabled(){
  return array('class' => 'custom-field', 'disabled' => 'disabled');
}



/* ------------------------------------------------- Page */



function _fluid_page_width_options(){
  return array(
    'auto' => t('Auto'),
    '625' => t('625 (640x480)'),
    '785' => t('785 (800x600)'),
    '1009' => t('1009 (1024x768)'),
    '1425' => t('1425 (1440x900)'),
    'custom' => t('custom'),
  );
}



/* ------------------------------------------------- Column */



function _fluid_column_width_options(){
  return array(
    '150' => t('150px'),
    '175' => t('175px'),
    '200' => t('200px'),
    '225' => t('225px'),
    'custom' => t('custom'),
  );
}

function _fluid_column_position_options(){ 
  return array(
    'left' => t('left side'),
    'right' => t('right side'),
    'both' => t('both sides'),
  );
}


/* ------------------------------------------------- Border */



function _fluid_border_elements(){
  return array(
    'top_left' => array(
      'position' => 'left top',
      'repeat' => 'no-repeat',
    ),
    'top_right' => array(
      'position' => 'right top',
      'repeat' => 'no-repeat',
    ),
    'top' => array(
      'position' => 'top',
      'repeat' => 'repeat-x',
    ),
    'left' => array(
      'position' => 'left',
      'repeat' => 'repeat-y',
    ),
    'right' => array(
      'position' => 'right',
      'repeat' => 'repeat-y',
    ),
    'container' => array(
      'position' => '',
      'repeat' => '',
    ),
    'bottom_left' => array(
      'position' => 'left bottom',
      'repeat' => 'no-repeat',
    ),
    'bottom_right' => array(
      'position' => 'right bottom',
      'repeat' => 'no-repeat',
    ),
    'bottom' => array(
      'position' => 'bottom', 
      'repeat' => 'repeat-x',
    ),
  );
}

function _fluid_border_options(){ 
  return array(
    'top_left' => 'top left',
    'top' => 'top',
    'top_right' => 'top right',
    'right' => 'right',
    'bottom_right' => 'bottom right',
    'bottom' => 'bottom',
    'bottom_left' => 'bottom left',
    'left' => 'left',
    'container' => 'inner container',
  );
}

function _fluid_border_order(){
  return array('top', 'right', 'bottom', 'left', 'top_left', 'top_right', 'bottom_right', 'bottom_left', 'container');
}