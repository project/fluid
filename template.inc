<?php
include_once('common.inc');



_bootstrap();



// ------------------------------------------------------------------- variables



/**
 * Implementation of hook_variables
 * Provides body classes & doctype tags
 */
function _phptemplate_variables($hook, $variables) {

  if ($hook=='page'){

    $theme_variables = _get_theme_variables();
    
    _html_body_vars($variables);
    
    _html_border_vars(
      $theme_variables, 
      $variables
    );
    
    _order_columns(
      $theme_variables, 
      $variables
    );

    _output_css(
      $theme_variables, 
      $variables
    );

    // Custom theming hook variables
    $variables['logo'] = theme('logo', $variables['logo']);
    $variables['shortcuts'] = 
      theme(
        'shortcuts', 
        $variables['primary_links'], 
        $variables['secondary_links']
      );
    $variables['breadcrumb'] = theme('breadcrumb', $variables['breadcrumb']);

    // Do script aggregation
    if(module_exists('javascript_aggregator') && $vars['scripts']) {
      $vars['scripts'] = javascript_aggregator_cache($vars['scripts']);
    }
  }
  
  if ($hook=='block'){

    $theme_variables = _get_theme_variables();
    
    $id = $variables['block']->subject;
    $id = trim(htmlspecialchars_decode($id, ENT_QUOTES));
    $id = ereg_replace('[\ ]+', '-', 
            ereg_replace('[^a-z0-9\ ]*', '', strtolower($id)));
    if ($id == '') {
      $id = 'block-'.$variables['block']->delta;
    }
    
    $variables['block']->id = $id;
    $variables['block_title_inside'] = 
      theme_get_setting($theme_variables['prefix'].'block_title_inside');
    _html_border($theme_variables, 'block', $variables);
  }
  
  return $variables;
}



/**
 * Renders the body class variable
 */
function _html_body_vars(&$variables){
    
  $body_classes = array();

  if ($variables['is_front']){
    $body_classes[] = 'front';
  }
  if ($variables['sidebar_left']){
    $body_classes[] = 'left-column';
    $body_classes['either'] = 'column';
  }
  if ($variables['sidebar_right']){
    $body_classes[] = 'right-column';
    $body_classes['either'] = 'column';
  }
  if ($variables['sidebar_left'] && $variables['sidebar_right']){
    $body_classes[] = 'columns';
  }
  
  if (arg(0) == 'admin'){
    $body_classes[] = 'admin';
  }
  
  if (count($body_classes)>0){
    $variables['body_class'] = ' class="'.implode(' ', $body_classes).'"';
  }
}



/**
 * Renders page element border html
 */
function _html_border_vars($theme_variables, &$variables){

  foreach (_fluid_custom_elements() as $element=>$element_info){
    if ($element_info['variable context'] == 'page'){
      _html_border($theme_variables, $element, $variables);
    }
  }
  
}



/**
 * Renders an elements' border and spacing html
 */
function _html_border($theme_variables, $element, &$variables){
    
  $prefix = FALSE;
  $suffix = FALSE;
  $count = 0;
  
  $element_key = ereg_replace('[^A-Za-z0-9\_]', '_', $element);
  $border_var_name = $element_key.'_border';
  $spacer_var_name = $element_key.'_spacer';
  
  $elements = $theme_variables['settings'][$theme_variables['prefix'].$border_var_name];
  
  foreach(_fluid_border_order() as $border_element_key){
    if ($elements[$border_element_key]){
      $prefix .= '<div class="border'.
        ($count==0 ? ' '.$element.'-outer ' : ' ').
        $element.'-'.str_replace('_', '-', $border_element_key).'">';
      $suffix .= '</div>';
      $count++;
    }
  }
  
  if ($count>0){
    $prefix = 
      substr($prefix, 0, strripos($prefix, 'border') + strlen('border')).
      ' '.$element.'-inner'.
      substr($prefix, strripos($prefix, 'border') + strlen('border'));
  }
  
  if (
        (
          _resolve_custom_setting(
            $theme_variables['settings'], 
            $theme_variables['prefix'].$element_key.'_margin') != '0px' 
          || (
            _resolve_custom_setting(
              $theme_variables['settings'], 
              $theme_variables['prefix'].$element_key.'_padding') != '0px' 
            &&
            $count == 0
          )
        ) 
        
        &! ($element == 'body')
        
    ){
    $variables[$spacer_var_name]['prefix'] = '<div class="spacing">';
    $variables[$spacer_var_name]['suffix'] = '</div>';
  }
  
  if ($prefix){
    $variables[$border_var_name]['prefix'] = $prefix;
    $variables[$border_var_name]['suffix'] = $suffix;
  }
}



/**
 * Renders CSS representing theme settings, 
 * and writes it to file or includes it in the header 
 */
function _output_css($theme_variables, &$variables){

  if ($theme_variables['settings'][$theme_variables['prefix'].'stylesheet']==1) {
    $path = file_directory_path().'/'.$theme_variables['key'];
    
    /* Generate stylesheet if it doesn't exist, or if the settings page has 
       been accessed */      
    if (variable_get($theme_variables['prefix'].'settings_accessed', FALSE)){
      _write_stylesheet(
        _css($theme_variables, $variables, TRUE),
        $path
      );
      variable_del($theme_variables['prefix'].'settings_accessed');
    }
    
    // Include the generated style
    drupal_add_css($path.'/dynamic.css', 'theme');
    
  } else { 
    // Include in page header
    $variables['dynamic_style'] = 
      '<style type="text/css" media="all">'."\n".
      _css($theme_variables, $variables, TRUE).
      "\n".'</style>'."\n";
  }
}



/**
 * Orders the columns for correct floating
 */
function _order_columns($theme_variables, &$variables){
  
  if ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'right'){
    $first_column = $variables['sidebar_right'];
    $second_column = $variables['sidebar_left'];
  } else {
    $first_column = $variables['sidebar_left'];
    $second_column = $variables['sidebar_right'];
  }

  if ($theme_variables['settings'][$theme_variables['prefix'].'column_swop']){
    $variables['first_column'] = $second_column;
    $variables['second_column'] = $first_column;
  } else {
    $variables['first_column'] = $first_column;
    $variables['second_column'] = $second_column;
  }
  
  $variables['content_title_inside'] = 
    $theme_variables['settings'][$theme_variables['prefix'].'content_title_inside'];
  if (
      $theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'left' || 
      $theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'right'){
      
    $variables['sidebar_wrapper']['prefix'] = 
      '<div id="sidebars">';
    $variables['sidebar_wrapper']['suffix'] = '</div>';
  }
  
}



// ---------------------------------------------------- css and script inclusion



/**
 * Fetches the browser user agent id, if set
 *
 * @return String user agent id
 */
function _browserAgent(){
  return (isset( $_SERVER['HTTP_USER_AGENT'] ) ) 
    ? strtolower( $_SERVER['HTTP_USER_AGENT'] ) 
    : '';
}



/**
 * Return TRUE if the $id occurs within $browserAgent
 *
 * @param $id
 * @param $browserAgent
 * @return unknown
 */
function _isAgent($id){
  return 
    (strlen($id) == 0) 
    || is_numeric(strpos(strtolower(_browserAgent()), strtolower(trim($id))));
}



/**
 * Automatic css & script inclusion
 */
function _includes($theme_variables){
  
  // Do the auto includes
  $include_path = $theme_variables['parent_path'].'/auto-includes';
  $sub_include_path = $theme_variables['path'].'/auto-includes';
  _include_dirs($sub_include_path, $include_path);

  if (
      strpos(' '.$includes_path, $theme_key) == 0 && 
      file_exists($subtheme_includes_path)){
    $includes_path = $subtheme_includes_path;
  }
    
  // Include the generated stylesheet
  if ($theme_variables['settings'][$theme_variables['prefix'].'stylesheet']==1) {
    drupal_add_css(file_directory_path().'/'.$theme_variables['key'].'/dynamic.css', 'theme', 'screen', FALSE);
  }
  
  drupal_get_css($css);
}



function _include_dirs($sub_path, $parent_path){
  $pathes = func_get_args();
  $added = array();
  
  for ($a=0; $a<2; $a++){
    if (file_exists($pathes[$a])){
      $includes_dir = opendir($pathes[$a]);
      
      while (false !== ($file = readdir($includes_dir))) {
        if ($file!='.' && $file!='..' && $added[$file] != 1){
  
          $fitsKeys = TRUE;
  
          if ((substr($file, 0, 5) == 'just-') &!
              ($file == 'just-front.css' && drupal_is_front_page())){
            // Check each search key, seperated by '-'
            $keys = explode('-', substr($file, 5, strlen($file) - 9));
            foreach($keys as $key){
              $fitsKeys &= _isAgent($key);
            }
          }
          
          if (
              substr($file, 0, 5) != 'just-' || 
              $fitsKeys && substr($file, 0, 5) == 'just-'){
            $added[$file] = 1;
            if (TRUE == (strpos($file, '.css') == strlen($file) - 4)){
              drupal_add_css(
                $pathes[$a].'/'.trim($file), 'theme', 'screen', FALSE
              );
            } elseif (TRUE == (strpos($file, '.js') == strlen($file) - 3)){
              drupal_add_js($pathes[$a].'/'.$file, 'theme');
            }
          }
        }
      }
    }

  }
}


// -------------------------------------------------------------- Theme settings



/**
 * Imports, exports or sets up default settings
 */
function _bootstrap(){
  
  $theme_variables = _get_theme_variables();
  
  $set_up = isset(
    $theme_variables['settings'][$theme_variables['prefix'].'body_border']
  );

  $port_settings_path = file_directory_path().'/fluid.settings';
  $to_export = 
    $theme_variables['settings'][$theme_variables['prefix'].
    'export_settings'] == '1';
  $to_import = 
    $theme_variables['settings']
                    [$theme_variables['prefix'].'import_settings'] == '1' && 
    file_exists($port_settings_path);

  $preset_settings_path = $theme_variables['path'].'/settings/preset.settings';
  $preset = file_exists($preset_settings_path);

  $template_inc_path = $theme_variables['path'].'/template.inc';
  $has_template = file_exists($template_inc_path);
  
  $firebug = 
    $theme_variables['settings']
    [$theme_variables['prefix'].'firebug'] == '1';
  
  // include child template.inc
  if ($has_template && $theme_variables['path'] != $theme_variables['parent_path']){
    include_once($template_inc_path);
  }
  
  // Export theme settings
  if ($to_export && $set_up){
    // Write settings to file
    file_put_contents(
      $port_settings_path, 
      serialize(
        _sanitize_settings($theme_variables['prefix'], 
        $theme_variables['settings']))
      );    
    // Lets not do it again
    unset(
      $theme_variables['settings']
                      [$theme_variables['prefix'].'export_settings']
    );
    _save_settings($theme_variables['key'], $theme_variables['settings']);
    drupal_set_message(
      'Fluid settings have been exported to the files folder.', 'status'
    );
  }
  
  // Import theme settings from files folder
  if ($to_import && !$preset){
    // Read settings from file
    $theme_variables['settings'] = 
      unserialize(file_get_contents($port_settings_path));
    // Lets not do it again
    unset(
      $theme_variables['settings'][$theme_variables['prefix'].'export_settings']
    );
    _save_settings($theme_variables['key'], $theme_variables['settings']);
    drupal_set_message(
      'Fluid settings have been imported from the files folder.', 'status'
    );
  }
  
  // Import presets
  if ($preset && !$set_up){
    // Read settings from file
    $theme_variables['settings'] = 
      unserialize(file_get_contents($preset_settings_path));
    _save_settings($theme_variables['key'], $theme_variables['settings']);
    drupal_set_message('Fluid presets have been loaded.', 'status');
  }
  
  // Setup default theme settings
  if (!$set_up && !$preset){
    _set_defaults(
      $theme_variables['key'], 
      $theme_variables['path'], 
      $theme_variables['prefix']
    );
  }
  
  // do css and javascript includes
  _includes($theme_variables);
  
  // include firebug
  if ($firebug) {
      drupal_add_js($theme_variables['parent_path'].'/includes/pi.js');
      drupal_add_js($theme_variables['parent_path'].'/includes/firebug-lite.js');
  }
}



/**
 * Saves a set of settings
 */
function _save_settings($theme_key, $theme_settings){
    
  // Don't save the toggle_node_info_ variables.
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($theme_settings['toggle_node_info_' . $type]);
    }
  }
  
  // Save theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    $theme_settings
  );
  
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}



/**
 * Returns settings only related to this theme
 */
function _sanitize_settings($theme_prefix, $theme_settings){

  $sanitary = array();
  foreach ($theme_settings as $key=>$value){
    if (
        substr($key, 0, strlen($theme_prefix)) == $theme_prefix 
        && $key != $theme_prefix.'export_settings'
        && $key != $theme_prefix.'import_settings'
       ){
        
      $sanitary[$key] = $value;
    }
  }
  return $sanitary;
}



/**
 * Loads default values
 *
 */
function _set_defaults($theme_key, $theme_path, $theme_prefix){
   
  $theme_settings = array();
  $default_settings_path = realpath($theme_path.'/settings/default.settings');
  $theme_settings = unserialize(file_get_contents($default_settings_path));
    
  // Convert settings to current theme
  $source_prefix = strtolower('fluid_');
  if ($theme_prefix != $source_prefix)
  foreach($theme_settings as $key=>$setting){
    $new_key = ereg_replace($source_prefix, $theme_prefix, $key);
    if ($new_key != $key){
      $theme_settings[$new_key] = $theme_settings[$key];
      unset($theme_settings[$key]);
    }
  }
  
  variable_set($theme_key.'_settings_accessed', 'true');
  _save_settings($theme_key, $theme_settings);
  
  drupal_set_message('Fluid default settings have been loaded.', 'status');    
}



// --------------------------------------------------------------- Dynamic style



/**
 * Generates a css file for the theme settings
 */
function _write_stylesheet($style, $path){

  $real_path = realpath($path);
  
  if (!file_exists($path)){
    mkdir($path);
  }
  
  $file = fopen($path.'/dynamic.css', 'w+');
  fwrite($file, '/* File dynamically generated */');
  fwrite($file, $style);
  
  fclose($file);
}



/**
 * Resolves the value of a setting if it can include a custom value 
 */
function _resolve_custom_setting($theme_settings, $setting){
  $resolved = $theme_settings[$setting] == 'custom'
    ? $theme_settings[$setting.'_custom']
    : $theme_settings[$setting];
   
//  print '<pre>Getting ['.$setting.'] '.$resolved.'</pre><br/>';
  return $resolved; 
}



/**
 * Generates css representing the theme settings
 */
function _css($theme_variables, $variables){

  _css_layout_vars($theme_variables, $variables);
  _css_border_vars($theme_variables, $variables);
  _css_color_vars($theme_variables, $variables);

  $style = 
    _phptemplate_default(
      '', 
      $variables, 
      array('includes/color'), 
      '.css.php')."\n".
    _phptemplate_default(
      '', 
      $variables, 
      array('includes/structure'), 
      '.css.php');

  return $style;
}



/**
 * Adds layout variables
 */
function _css_layout_vars($theme_variables, &$variables){
    
  $page_width = 
    str_replace('px', '', 
      _resolve_custom_setting(
        $theme_variables['settings'], 
        $theme_variables['prefix'].'page_width'
      )
    );
  $column_width = 
    str_replace('px', '', 
      _resolve_custom_setting(
        $theme_variables['settings'], 
        $theme_variables['prefix'].'column_width'
      )
    );  
  $left_column_width = $variables['sidebar_left'] ? $column_width : 0;
  $right_column_width = $variables['sidebar_right'] ? $column_width : 0;
  
  // Widths
  $variables['page_width'] = $page_width;
  $variables['column_width'] = $column_width;
  $variables['column_container_width'] = 
    $left_column_width + $right_column_width;
  $variables['custom_width'] = (int)($page_width != 'auto');
  
  // Floats
  $variables['left_column_float'] = 
    ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'left' || 
     $theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'both'
      ? 'left'
      : 'right'
    );
  $variables['right_column_float'] = 
    ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'right' || 
     $theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'both'
      ? 'right'
      : 'left'
    );
  $variables['sidebars_float'] = 
    ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'both'
      ? FALSE
      : $theme_variables['settings'][$theme_variables['prefix'].'column_position']
    );
  
  // Margins
  $variables['content_layout_margin'] = 
    ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'left'
      ? '0px 0px 0px '.($left_column_width + $right_column_width).'px'
      : ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'right'
        ? '0px '.($left_column_width + $right_column_width).'px 0px 0px'
        : '0px '.$right_column_width.'px 0px '.$left_column_width.'px')
    );    
  $variables['sidebars_margin'] = 
    ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'left'
      ? '0px '.-($left_column_width + $right_column_width).'px 0px 0px'
      : ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'right'
        ? '0px 0px 0px '.-($left_column_width + $right_column_width).'px'
        : '0px')
    );
  $variables['first_column_margin'] = 
    ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'both'
      ? '0px '.-$column_width.'px 0px 0px'
      : FALSE
    );
  $variables['second_column_margin'] = 
    ($theme_variables['settings'][$theme_variables['prefix'].'column_position'] == 'both'
      ? '0px 0px 0px -'.$column_width.'px'
      : FALSE
    );
}



/**
 * Adds border, margin & padding variables
 */
function _css_border_vars($theme_variables, &$variables){
  
  foreach(_fluid_custom_elements() as $element_key=>$element_info){

    $element_key = ereg_replace('[^A-Za-z0-9\_]', '_', $element_key);
    
    // Borders
    if ($element_info['border']){
      $variables[$element_key.'_border'] = FALSE;
      
      $var_name = ereg_replace('[^A-Za-z0-9\_]', '_', $element_key).'_border';
      $elements = $theme_variables['settings'][$theme_variables['prefix'].$var_name];
      
      $border_elements = _fluid_border_elements();
            
      foreach($border_elements as $border_element_key=>$border_element_info){
        if ($elements[$border_element_key]){
          
          if (!$variables[$element_key.'_border']) {
            $variables[$element_key.'_border'] = array();
          }

          $replaced_border_element_key = 
            str_replace('_', '-', $border_element_key);
          $replaced_element_key = str_replace('_', '-', $element_key);
          
          $url = 
            $theme_variables['path'].
            '/images/border/'.
            $replaced_element_key.'-'.
            $replaced_border_element_key.'.png';
          $url = file_exists($url)
            ? '/'.$url
            : '/'.$variables['parent_path'].
              '/images/border/default-'.$replaced_border_element_key.'.png';

          $variables[$element_key.'_border'][$replaced_border_element_key] = 
            array(
              'url' => $url,
              'repeat' => $border_elements[$border_element_key]['repeat'],
              'position' => $border_elements[$border_element_key]['position'],
            );
        }
      }
      
    }
    
    // Margins
    if ($element_info['margin']){
      $variables[$element_key.'_margin'] = ($element_info['margin']
        ? $variables[$element_key.'_margin'] = 
          _resolve_custom_setting(
            $theme_variables['settings'], 
            $theme_variables['prefix'].$element_key.'_margin'
          )
        : FALSE);
    }
    
    // Padding
    if ($element_info['padding']){
      $variables[$element_key.'_padding'] = ($element_info['padding']
        ? $variables[$element_key.'_padding'] = 
          _resolve_custom_setting(
            $theme_variables['settings'], 
            $theme_variables['prefix'].$element_key.'_padding'
          )
        : FALSE);
    }
  }
//  print '<pre>'; print_r($variables); print '</pre>';
}



/**
 * Calculates color css variables
 */
function _css_color_vars($theme_variables, &$variables){
  
  $variables['color_elements'] = array();
  
  foreach (_fluid_custom_elements() as $element_key=>$element_info){
    if ($element_info['color']){
      
      foreach ($element_info['color'] as $element=>$info){
        $element_key_replaced = str_replace('-', '_', $element_key);
        if ($theme_variables['settings'][$theme_variables['prefix'].
                            $element_key_replaced.'_use_'.$element.'_color']){
          $variables['color_elements']
              [$element_key]
              [$info['selector']]
              [$info['property']]
            = $theme_variables['settings'][$theme_variables['prefix'].
                              $element_key_replaced.'_'.$element.'_color']; 
        }
      }
      
    }   
  }
  
  return $variables;
}