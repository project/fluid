<?php



// ---------------------------------------------------------------- View content



/**
 * Constructs a list of the view items
 * Stolen and modified from theming wizard
 */
function _get_view_content(
          $template, $view, $nodes, $type, $group_by, $label_key){
            
  $fields = _views_get_fields();
  $taken = array();

  // Set up the fields in nicely named chunks.
  foreach ($view->field as $id => $field) {
    $field_name = $field['field'];
    
    if (isset($taken[$field_name])) {
      $field_name = $field['queryname'];
    }

    $taken[$field_name] = true;
    $field_names[$id] = $field_name;
  }

  // Set up some variables that won't change.
  $base_vars = array(
    'view' => $view,
    'view_type' => $type,
  );
  
  if (!$group_by){
    
    return _build_item_list(
      $nodes, $base_vars, $view, $field_names, $fields, $template);
    
  } else {

    return _build_grouped_item_list(
      $nodes, $base_vars, $view, $field_names, $fields, $template, 
      $group_by, $label_key
    );
    
  }
}



/**
 * Builds a list of themed items
 */
function _build_item_list(
          $nodes, $base_vars, $view, $field_names, $fields, $template){
            
  $items = array();
  $node_count = count($nodes);
  
  foreach ($nodes as $i => $node) {
    $vars = $base_vars;
    $vars['node'] = $node;
    $vars['count'] = $i;
    $vars['stripe'] = $i % 2 ? 'even' : 'odd';
    $vars['last'] = ($i == ($node_count - 1));
    $vars['first'] = ($i == 0);
    
    foreach ($view->field as $id => $field) {
      $name = $field_names[$id];
      
      $vars[$name] = views_theme_field(
        'views_handle_field', 
        $field['queryname'], 
        $fields, $field, $node, $view
      );
      
      if (isset($field['label'])) {
        $vars[$name . '_label'] = $field['label'];
      }
    }
    
    $items[] = _callback($template, $vars);
  }
 
  return $items;
}



/**
 * Builds a grouped list of themed items
 */
function _build_grouped_item_list(
          $nodes, $base_vars, $view, $field_names, $fields, $template, 
          $group_by, $label_key){
            
  $set = array();
  $taken = array();
  $items = array();
  $node_count = count($nodes);
  
  // Group our nodes
  foreach ($nodes as $node) {
    $group = (array)$node;
    $set[$group[$group_by]][] = $node;
  }

  foreach ($set as $label => $nodes) {
    $sub_items = array ();
    
    foreach ($nodes as $i => $node) {
      $vars = $base_vars;
      $vars['node'] = $node;
      $vars['count'] = $i;
      $vars['stripe'] = $i % 2 ? 'even' : 'odd';
      $vars['last'] = ($i == ($node_count - 1));
      $vars['first'] = ($i == 0);
      
      foreach ($view->field as $id => $field) {
        $name = $field_names[$id];
        
        $vars[$name] = views_theme_field(
          'views_handle_field', 
          $field['queryname'], 
          $fields, $field, $node, $view
        );
        
        if (isset ($field['label'])) {
          $vars[$name . '_label'] = $field['label'];
        }
      }
      
      $sub_items[] = _callback($template, $vars);
    }
    
    if ($sub_items) {
      $items[] = theme(
        'item_list', 
        $sub_items, 
        (empty($label_key)?$label:$vars[$label_key])
      );
    }
  }

  return $items;
}



/**
 * Performs a php_template callback
 *
 */
function _callback($template, $vars){
  global $theme;
  global $theme_key;
  
  if ($theme != $theme_key){
    return _phptemplate_callback($template, $vars, array('sub-themes/'.$theme_key.'/'.$template));
  } else {
    return _phptemplate_callback($template, $vars);
  }
}

// --------------------------------------------------------------- View themeing



/*
 * Return view content concatenated
 */
function _view_concatenated(
          $template, $view, $nodes, $type, 
          $group_by = FALSE, $label_key = NULL) {
            
  $items = 
    _get_view_content($template, $view, $nodes, $type, $group_by, $label_key);
  
  if ($items) {
    return implode("\n", $items);
  }
}



/*
 * Return view content themed as an item list
 */
function _view_as_item_list(
          $template, $view, $nodes, $type, 
          $group_by = FALSE, $label_key = NULL) {
            
  $items = 
    _get_view_content($template, $view, $nodes, $type, $group_by, $label_key);
    
  if ($items) {
    return theme('item_list', $items);
  }
}

/*
 * Return view content themed as an ordered list
 */
function _view_as_ordered_list(
          $template, $view, $nodes, $type, 
          $group_by = FALSE, $label_key = NULL) {
            
  $items = 
    _get_view_content($template, $view, $nodes, $type, $group_by, $label_key);
    
  if ($items) {
    return theme('item_list', $items, NULL, 'ol');
  }
}

/*
 * Return view content themed as a table <shudder>
 */
function _view_as_table(
          $template, $view, $nodes, $type, 
          $group_by = FALSE, $label_key = NULL) {
  
  $items = 
    _get_view_content($template, $view, $nodes, $type, $group_by, $label_key);
    
  if ($items) {
    return 
      '<table>'.implode($items).'</table>';
  }
}