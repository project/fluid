<?php
include_once('common.inc');


/**
 * Implementation of hook_settings
 */
function fluid_settings($saved_settings) {
  
  $theme_variables = _get_theme_variables();
  
  include_once($theme_variables['parent_path'].'/theme-settings.inc');
  
  $preset_settings_path = $theme_variables['path'].'/settings/preset.settings';
  $preset = file_exists($preset_settings_path);
  
  if (!$preset){

    drupal_add_js(
      $theme_variables['parent_path'].'/includes/theme-settings.js', 'theme'
    );
    drupal_add_js(
      $theme_variables['parent_path'].'/includes/farbtastic.js', 'theme'
    );
    drupal_add_css(
      $theme_variables['parent_path'].'/includes/farbtastic.css', 'theme'
    );
    
    $form = array();
    $form[] = array(
      '#value' => '<br/>',
    );

    $default_settings_path = 
      $theme_variables['path'].'/settings/default.settings'; 
    $default_settings = unserialize(file_get_contents($default_settings_path));
    $theme_variables['settings'] = array_merge(
      $default_settings, $saved_settings
    );
    $options = _build_numeric_options(
      $theme_variables['prefix'], $theme_variables['settings']
    );
    
    _build_shared(
      $theme_variables['prefix'], $theme_variables['settings'], $options, $form
    );
    _build_page(
      $theme_variables['prefix'], $theme_variables['settings'], $form
    );
    _build_column(
      $theme_variables['prefix'], $theme_variables['settings'], $form
    );
    _build_preferences(
      $theme_variables['prefix'], $theme_variables['settings'], $options, $form
    );
    _build_persistence(
      $theme_variables['prefix'], $theme_variables['settings'], $form
    );
    
    // Flag the stylesheet to be regenerated
    variable_set($theme_variables['key'].'_settings_accessed', 'true');
    return $form;
  }
}