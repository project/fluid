<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $dynamic_style ?>
  <?php print $scripts; ?>
 </head>

<body <?php print $body_class; ?>>

  <?php print $body_border['prefix']; ?>

    <div id="center">
      <div id="page">
        <?php print $page_border['prefix']; ?>
  
          <div id="page-header">
            <?php print $header_spacer['prefix']; ?>
              <?php print $header_border['prefix']; ?>
                <?php print $logo; ?>
                <?php print $shortcuts; ?>
                <?php print $header; ?>
                <?php print $breadcrumb; ?>
              <?php print $header_border['suffix']; ?>
            <?php print $header_spacer['suffix']; ?>
          </div><!-- /page-header -->
            
          <div id="page-body">
            <?php print $page_body_spacer['prefix']; ?>
              <?php print $page_body_border['prefix']; ?>
                <div id="page-body-columns" class="float-container">
  
                  <?php print $sidebar_wrapper['prefix']; ?>
                    <?php if ($first_column): ?>
                    <div id="first-column" class="side">
                      <?php print $column_spacer['prefix']; ?>
                        <?php print $column_border['prefix']; ?>
                          <?php print $first_column; ?>
                        <?php print $column_border['suffix']; ?>
                      <?php print $column_spacer['suffix']; ?>
                    </div><!-- /first-column -->
                    <?php endif; ?>
                    
                    <?php if ($second_column): ?>
                    <div id="second-column" class="side">
                      <?php print $column_border['prefix']; ?>
                        <?php print $column_spacer['prefix']; ?>
                          <?php print $second_column; ?>
                        <?php print $column_spacer['suffix']; ?>
                      <?php print $column_border['suffix']; ?>
                    </div><!-- /second-column -->
                    <?php endif; ?>
                  <?php print $sidebar_wrapper['suffix']; ?>
                  
                  <div id="content">
                    <?php print $content_spacer['prefix']; ?>
                      <?php if ($title && !$content_title_inside): ?><div id="title"><h1 class="title"><?php print $title; ?></h1></div><?php endif; ?>
                      <?php print $content_border['prefix']; ?>
                      
                        <div class="float-container">
                          <div class="float-localiser">
                            <?php if ($content_top): ?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
                            <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
                            <?php if ($title && $content_title_inside): ?><div id="title"><h1 class="title"><?php print $title; ?></h1></div><?php endif; ?>
                            <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
                            <?php print $messages; ?>
                            <?php print $help; ?>
                            <?php print $content; ?>
                            <?php print $feed_icons; ?>
                            <?php if ($content_bottom): ?><div id="contentBottom"><?php print $content_bottom; ?></div><?php endif; ?>
                          </div>
                        </div>
                      <?php print $content_border['suffix']; ?>
                    <?php print $content_spacer['suffix']; ?>
                  </div><!-- /content -->
                  
                </div><!-- /page-body-columns -->
              <?php print $page_body_border['suffix']; ?>
            <?php print $page_body_spacer['suffix']; ?>
          </div><!-- /page-body -->
            
          <?php if ($footer_message): ?>
          <div id="page-footer">
            <?php print $footer_border['prefix']; ?>
              <?php print $footer_message; ?>
            <?php print $footer_border['suffix']; ?>
          </div>
          <?php endif; ?>
            
        <?php print $page_border['suffix']; ?>
      </div><!-- /page -->
    
      <div id="extra-regions">
        <?php if ($extra_0) :?><div id="extra-0"><?php print $extra_0; ?></div><?php endif; ?>
        <?php if ($extra_1) :?><div id="extra-1"><?php print $extra_1; ?></div><?php endif; ?>
      </div>
      
    </div><!-- /center -->
  
  <?php print $body_border['suffix'] ?>
  
  <?php print $closure ?>
</body>
</html>