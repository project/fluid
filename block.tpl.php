<div id="<?php print $css_id; ?>" class="block"><!-- block -->
  <?php if ($block->subject && !$block_title_inside && $show_title): ?><h2 class="title"><?php print $block->subject; ?></h2><?php endif;?>
  <?php print $block_border['prefix']; ?>
  <?php if ($block->subject && $block_title_inside && $show_title): ?><h2 class="title"><?php print $block->subject; ?></h2><?php endif;?>
  <div class="content"><?php print $block->content; ?></div>
  <?php print $block_border['suffix']; ?>
</div><!-- /block -->
